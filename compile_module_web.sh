#!/bin/bash
. config_web.sh

echo -n 1 > .compiling_in_progess

./build_module.sh

cd build

if [ "$type" = "sftp" ]; then

env SSHPASS="$password" sshpass -e sftp -oBatchMode=no -P "$port" "$username@$address" <<END
cd "$path"
get scenes.txt
put *.txt
put -r ../lua
put ../brf/*.brf Resource/
END

elif [ "$type" = "ftp" ]; then

ftp -np <<EOF
prompt n
open $address
user $username $password
cd "$path"
get scenes.txt
mput *.txt
EOF

elif [ "$type" = "lftp" ]; then

lftp "$address" <<EOF
set xfer:clobber on
get scenes.txt
mput *.txt
EOF

fi

cd ..

echo -n 0 > .compiling_in_progess
