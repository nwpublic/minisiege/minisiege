#!/bin/bash
source config_shell.sh

echo Downloading configs...

sftp -P "$live_port" "$live_address" <<END

cd "$live_path"

get config_*.py

END
