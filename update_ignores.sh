#!/bin/bash

for file in const_* config_*; do
    if [ "$1" = "undo" ]; then
        git update-index --no-assume-unchanged "$file"
    else
        git update-index --assume-unchanged "$file"
    fi
done
