game.display_message("LUA loaded")

function handleURL(url)
    if url:find("/mb/log") 
    and (
        url:find("kills") or -- also teamkills
        url:find("deaths")
    ) then
        file = io.open("queries.txt", "a+b")
        file:write(url .. "\n")
        file:close()
        
        return false
    end
end

game.hookOperation("send_message_to_url", handleURL)


--[[

function echoMessages(str)
    game.str_store_string(1, str)
    local no = game.getScriptNo("broadcast_admin_info_s1")
    game.call_script(no)
end
game.hookOperation("display_message", echoMessages)

--]]--

